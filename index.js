async function getPokedex() {
  let response = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=151`);
  let data = await response.json();
  const pokemonList = data.results;
  return pokemonList;
}

var getId = (pokemon) => {
  const id = pokemon.url
    .replace("https://pokeapi.co/api/v2/pokemon/", "")
    .replace("/", "");
  return id;
};

var getName = (pokemon) => {
  return pokemon.name;
};

var getType = (data) => {
  const types = [];

  for (let i = 0; i < data.types.length; i++) {
    types.push(data.types[i].type.name);
  }
  return types;
};

var displayPokedex = async () => {
  //! Display pokemon data to main page
  cleanPokedex();
  const pokemonList = await getPokedex();
  const pokedexDiv = document.querySelector(".pokedex-div");

  console.log(pokemonList);

  pokemonList.forEach((pokemon) => {
    const id = getId(pokemon);
    const name = getName(pokemon);

    const pokemonDiv = document.createElement("div");
    const pId = document.createElement("p");
    const pName = document.createElement("p");

    pokedexDiv.appendChild(pokemonDiv);

    pokemonDiv.classList.add("pokemon-div");
    pokemonDiv.classList.add("pokemon-div" + id);
    pokemonDiv.appendChild(pId);
    pId.classList.add("pokemon-id");
    pId.innerHTML = `Nº <span class="id">${id}</span>`;

    pokemonDiv.addEventListener("click", function () {
      searchPokemonByInput(id);
    });

    pokemonDiv.appendChild(pName);
    pName.innerHTML = name;
    pName.classList.add("pokemon-name");

    /*if (id > 30) {
      pokemonDiv.style.display = "none";
    }*/
  });
};

var backgroundType = (id, type) => {
  const pokemonDiv = document.querySelector(".pokemon-div" + id);
  const pokedexDiv = document.querySelector(".pokedex-div");
  pokedexDiv.style.display = "flex";

  pokemonDiv.style.cssText = `
    background-image:url("style/img/${type}Type.jpg");
    background-repeat:no-repeat;
    background-size: cover;
    background-position: center center;
    width:80vmin;
    

    `;
};

async function pokemonFoundDetails(id) {
  if (!document.querySelector(".image-" + id)) {
    let response = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
    let data = await response.json();
    let types = getType(data);
    let urlImg = data.sprites.front_default;

    const pokemonDiv = document.querySelector(".pokemon-div" + id);
    const image = document.createElement("img");
    const ul = document.createElement("ul");

    image.src = urlImg;
    pokemonDiv.appendChild(image);
    image.classList.add("image-" + id);
    pokemonDiv.appendChild(ul);

    types.forEach((type) => {
      const li = document.createElement("li");
      li.innerHTML = type;
      ul.appendChild(li);
      li.classList.add("li-type-" + type);
    });

    backgroundType(id, types[0]);
  }
}

var searchPokemonByInput = (id) => {
  //! Search pokemon details by input value
  if (id == undefined) {
    const searchedPokemon = document.querySelector(".pokemon-name-input").value;

    findPokemonDiv(searchedPokemon);
  } else {
    const pokemon = document.querySelector(".pokemon-div" + id);
    const searchedPokemon = pokemon.querySelector(".pokemon-name").innerHTML;
    findPokemonDiv(searchedPokemon);
  }
};

var findPokemonDiv = (searchedPokemon) => {
  const pokedexDiv = document.querySelector(".pokedex-div");
  pokedexDiv.style.gridTemplateColumns = "1fr";
  const pokemonDivName = document.querySelectorAll(".pokemon-name");

  pokemonDivName.forEach((pokemonName) => {
    if (searchedPokemon !== pokemonName.innerHTML) {
      pokemonName.parentElement.style.display = "none";
    } else {
      pokemonName.parentElement.style.display = "inline";
      const pokemonId =
        pokemonName.parentElement.querySelector(".id").innerHTML;
      console.log(pokemonId);
      //pokemonFoundStyle();

      pokemonFoundDetails(pokemonId);
    }
  });
};

var displayNewPokedex = () => {
  const pokedexDiv = document.querySelector(".pokedex-div");

  cleanPokedex();
  pokedexDiv.style.gridTemplateColumns = "repeat(2, 1fr)";
  pokedexDiv.style.display = "grid";
  displayPokedex();
};

async function randomPokemon() {
  let max = 151;
  let min = 1;
  const id = Math.floor(Math.random() * (max - min + 1)) + min;
  let response = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
  let data = await response.json();
  findPokemonDiv(data.name);
}

var cleanPokedex = () => {
  const pokemonDiv = document.querySelectorAll(".pokemon-div");

  pokemonDiv.forEach((children) => {
    children.remove();
  });
};

displayPokedex();

/*const delay = (ms) => new Promise((res) => setTimeout(res, ms));

window.onscroll = async function (ev) {
  const pokemonDiv = document.querySelectorAll(".pokemon-div");
  if (window.innerHeight + window.scrollY >= document.body.scrollHeight) {
    var counter = 0;
    pokemonDiv.forEach((div) => {
      if (div.style.display !== "none") {
        counter += 1;
      }
    });

    if (counter <= 121) {
      const loadDiv = document.querySelector(".load-data");

      loadDiv.style.display = "inline";
      await delay(1000);
      for (let i = counter + 1; i <= counter + 30; i++) {
        const pokemonDiv = document.querySelector(`.pokemon-div${i}`);
        pokemonDiv.style.display = "inline";
      }
      loadDiv.style.display = "none";
    }
  }
};*/
